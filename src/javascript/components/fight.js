import { controls } from '../../constants/controls';
import { getRandomFloat } from '../helpers/randomHelper';
import { pressedKeys, PressedKeysService } from '../services/pressedKeysService';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {

    firstFighter.state = getStartFighterState(firstFighter, 'left');
    secondFighter.state = getStartFighterState(secondFighter, 'right');

    new PressedKeysService({
      [controls.PlayerOneAttack]: () => attack(firstFighter, secondFighter, resolve),
      [controls.PlayerTwoAttack]: () => attack(secondFighter, firstFighter, resolve),
      [controls.PlayerOneCriticalHitCombination]: () => critAttack(firstFighter, secondFighter, resolve),
      [controls.PlayerTwoCriticalHitCombination]: () => critAttack(secondFighter, firstFighter, resolve)
    });
  });
}

function attack(attacker, defender, resolve) {
  if (attacker.state.isBlocked()) return;
  const { state } = defender;
  state.health -= state.isBlocked() ? 0 : getDamage(attacker, defender);
  if (state.health <= 0) {
    state.health = 0;
    resolve(attacker);
  }
  setFightersHealthbars(attacker, defender);
}

function critAttack(attacker, defender, resolve) {
  if (!attacker.state.canCrit) return;
  attacker.state.canCrit = false;
  setTimeout(() => attacker.state.canCrit = true, 10000);
  const { state } = defender;
  state.health -= attacker.attack * 2;
  if (state.health <= 0) {
    state.health = 0;
    resolve(attacker);
  }
  setFightersHealthbars(attacker, defender);
}

function getStartFighterState(fighter, position) {
  const { health } = fighter;
  return {
    health,
    position,
    canCrit: true,
    isBlocked: () => pressedKeys.has(position === 'left' ? controls.PlayerOneBlock : controls.PlayerTwoBlock)
  }
}

function setFightersHealthbar(curHealth, maxHealth, position) {
  const id = `${position}-fighter-indicator`;
  const indicator = document.getElementById(id);
  indicator.style.width = `${curHealth / maxHealth * 100}%` 
}

function setFightersHealthbars(firstFighter, secondFighter) {
  setFightersHealthbar(firstFighter.state.health, firstFighter.health, firstFighter.state.position)
  setFightersHealthbar(secondFighter.state.health, secondFighter.health, secondFighter.state.position)
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(damage, 0);
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = getRandomFloat(1, 2);
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = getRandomFloat(1, 2);
  const power = defense * dodgeChance;
  return power;
}

