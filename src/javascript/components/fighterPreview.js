import { createElement } from '../helpers/domHelper';
import { capitalizeFirstLetter } from '../helpers/strHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterImage.classList.add(positionClassName);
    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter___info'
  })

  appendInfoToElement(infoElement, { name, health, attack, defense })

  return infoElement;
}

function appendInfoToElement(element, obj) {
  for (const key in obj) {
    const span = createElement({ tagName: 'span' });
    const strong = createElement({ tagName: 'strong' });
    const p = createElement({ tagName: 'p' });
    strong.innerText = `${capitalizeFirstLetter(key)}:`;
    p.innerText = obj[key];
    span.appendChild(strong);
    span.appendChild(p);
    element.appendChild(span);
  }
}