import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const { name } = fighter;
  const title = `${name} won!`;
  const bodyElement = createModalWindowBodyElement();
  const onClose = () => {
    location.reload();
  }
  showModal({
    title,
    bodyElement,
    onClose
  });
}

function createModalWindowBodyElement() {
  const body = createElement({
    tagName: 'div',
    className: 'modal-body'
  })
  const p = createElement({ tagName: 'p' });
  p.innerText = 'Congratulations! Close this window to fight again!'
  const img = createElement({ 
    tagName: 'img',
    attributes: {
      src: './resources/ko.png',
      alt: 'K.O.'
    }
   });
   body.appendChild(p);
   body.appendChild(img);
  return body;
}