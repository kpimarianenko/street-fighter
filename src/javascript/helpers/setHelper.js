export function setHasArr(set, arr) {
  let has = false;
  for (let i = 0; i < arr.length; i++) {
    if (set.has(arr[i])) has = true;
    else {
      has = false;
      break;
    }
  }
  return has;
}