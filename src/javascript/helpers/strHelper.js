export function capitalizeFirstLetter(str) {
    return str[0].toUpperCase() + str.substring(1);
}